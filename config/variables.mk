circos = tools/opt/circos/bin/circos
bwa = tools/opt/bwa/bwa

mtdna = $(proot)/data/mtdna
tracks = $(proot)/tracks
tmp = $(proot)/make
reference = $(proot)/data/sp_AXEQ-consensus.fa

southern_ids = $(shell cd $(mtdna) && ls -1 ?.fa | awk -F'.' '{print $$1}')
northern_ids = SPA1
ext_sp_ids = pubmed-9707584
ext_sc_ids = Scu-MCZ12413 Scu-MFN334-SEG_AY530087S Scu-MFN334-AY530087S1 Scu-MFN334-AY530087S2 Scu-MFN334-SEG_AY530085S Scu-MFN334-AY530085S2 Scu-MFN3320 Scu-MFN334-AY530085S1 
all_ids = $(southern_ids) $(northern_ids) $(ext_sp_ids) $(ext_sc_ids)

sanger_ids = 1 2 3 T

track_xmls = $(shell tools/make/subst "tracks/%.xml" "$(all_ids)")
sanger_xmls = $(shell tools/make/subst "tracks/sanger/%.xml" "$(sanger_ids)")
