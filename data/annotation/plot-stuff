#!/usr/bin/python3
from sys import argv, exit
from collections import defaultdict
from matplotlib import use; use("Agg")
import numpy
from scipy import stats
import matplotlib.pyplot as plot

translate = {
    "TTT": "Phe", "TTC": "Phe", "TTA": "Leu", "TTG": "Leu", "CTT": "Leu",
    "CTC": "Leu", "CTA": "Leu", "CTG": "Leu", "ATT": "Ile", "ATC": "Ile",
    "ATA": "Met", "ATG": "Met", "GTT": "Val", "GTC": "Val", "GTA": "Val",
    "GTG": "Val", "TCT": "Ser", "TCC": "Ser", "TCA": "Ser", "TCG": "Ser",
    "CCT": "Pro", "CCC": "Pro", "CCA": "Pro", "CCG": "Pro", "ACT": "Thr",
    "ACC": "Thr", "ACA": "Thr", "ACG": "Thr", "GCT": "Ala", "GCC": "Ala",
    "GCA": "Ala", "GCG": "Ala", "TAT": "Tyr", "TAC": "Tyr", "TAA": "***",
    "TAG": "***", "CAT": "His", "CAC": "His", "CAA": "Gln", "CAG": "Gln",
    "AAT": "Asn", "AAC": "Asn", "AAA": "Lys", "AAG": "Lys", "GAT": "Asp",
    "GAC": "Asp", "GAA": "Glu", "GAG": "Glu", "TGT": "Cys", "TGC": "Cys",
    "TGA": "Trp", "TGG": "Trp", "CGT": "Arg", "CGC": "Arg", "CGA": "Arg",
    "CGG": "Arg", "AGT": "Ser", "AGC": "Ser", "AGA": "***", "AGG": "***",
    "GGT": "Gly", "GGC": "Gly", "GGA": "Gly", "GGG": "Gly"
}

def pop_codons(fasta):
    for i in range(0, len(fasta), 3):
        yield fasta[i:i+3]

def get_frequencies(filename):
    frequencies = defaultdict(dict)
    fastas = [s.strip().upper() for s in open(filename).readlines()][1::2]
    for fasta in fastas:
        for codon in pop_codons(fasta):
            if codon in translate:
                aminoacid = translate[codon]
                frequencies[aminoacid][codon] = 1 + frequencies[aminoacid].get(codon, 0)
    for aminoacid in frequencies:
        pairs = list(frequencies[aminoacid].items())
        #pairs.sort(key=lambda x: x[1], reverse=True)
        pairs.sort()
        total = sum(p[1] for p in pairs)
        pairs = [(p[0], int(100*p[1]/total)) for p in pairs]
        frequencies[aminoacid] = pairs
    return frequencies

def import_data():
    solen = get_frequencies("s.paradoxus-cds.fa")
    hedge = get_frequencies("e.europaeus-mtdna-cds.fa")
    felis = get_frequencies("felis-catus-cds.fa")
    sorex = get_frequencies("sorex-araneus-cds.fa")
    talpa = get_frequencies("talpa-europaea-cds.fa")
    human = get_frequencies("human-cds.fa")
    by_species = defaultdict(dict)
    for aminoacid in translate.values():
        by_species[aminoacid]["solenodon"] = solen[aminoacid]
        by_species[aminoacid]["talpa"] = talpa[aminoacid]
        by_species[aminoacid]["sorex"] = sorex[aminoacid]
        by_species[aminoacid]["erinaceus"] = hedge[aminoacid]
        by_species[aminoacid]["felis"] = felis[aminoacid]
        by_species[aminoacid]["human"] = human[aminoacid]
    return by_species

if __name__ == "__main__":
    figure = plot.figure()
    subplot = figure.add_subplot(111)
    by_species = import_data()
    species_list = ["solenodon", "talpa", "sorex", "erinaceus", "felis", "human"][::-1]
    species_list = ["human", "felis", "solenodon", "talpa", "erinaceus", "sorex"]
    M = len(species_list)
    MLIST = numpy.array(list(range(M)))
    TLIST = numpy.abs([76-92, 76-84, 0, 76-73, 76-65, 76-65])
    species_list = ["solenodon", "talpa", "felis", "erinaceus", "sorex", "human"]
    TLIST = numpy.array([0, 1, 8, 11, 11, 16])
    every = defaultdict(dict)
    for aminoacid, species_spectra in by_species.items():
        keys = []
        for species, spectra in species_spectra.items():
            for k, v in spectra:
                every[k][species] = v
    every_proper = defaultdict(dict)
    for aminoacid, D in every.items():
        L = numpy.array([D.get(species, 0) for species in species_list])
        every_proper[aminoacid] = L
    totals = {}
    for dangle, color in zip(list("ACGT"), list("rgb") + ["purple"]):
        keys = [key for key in every_proper.keys() if key[-1] == dangle]
        total = numpy.array([0] * M)
        for key in keys:
            total += every_proper[key]
        totals[dangle] = total
        #k, res, _, _, _ = numpy.polyfit(MLIST, total, 1, full=True)
        k, a, r, p, se = stats.linregress(TLIST, total)
        print(dangle, r**2)
        _ = plot.scatter(TLIST, total, color=color)
        _ = plot.plot(TLIST, total, color=color, ls=":")
        _ = plot.plot(TLIST, k * TLIST + a, color=color, lw=2)
    subplot.set_xticks(TLIST)
    subplot.set_xticklabels([""] + species_list)
    subplot.plot()
    figure.savefig("ha.png")
