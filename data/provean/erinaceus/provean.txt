>ATP6
125,L,T	-4.463
>ATP8
27,K,A	-5.860
62,L,H	-5.015
>COX1
>COX2
22,Y,G	-4.172
>COX3
>CYTB
>ND1
252,P,S	-5.302
308,P,H	-7.128
>ND2
11,M,N	-6.262
12,N,P	-6.067
13,P,L	-8.929
26,G,T	-7.197
34,S,H	-4.394
35,H,W	-9.005
36,W,L	-11.922
39,I,W	-4.633
40,W,I	-12.829
41,M,G	-4.502
42,G,F	-8.257
43,F,E	-6.104
44,E,M	-6.422
45,M,N	-6.099
46,N,T	-4.905
49,A,I	-4.231
51,I,P	-6.228
52,P,I	-9.161
55,M,K	-4.318
59,N,P	-6.275
61,R,S	-5.525
64,E,A	-5.522
68,K,Y	-6.448
70,F,L	-5.457
71,L,T	-4.284
73,Q,A	-5.527
75,T,A	-4.405
77,S,M	-4.606
86,I,N	-4.995
87,N,L	-8.429
92,G,H	-7.439
93,Q,W	-5.536
94,W,S	-12.892
152,Y,M	-6.348
156,P,N	-6.102
207,P,Q	-7.095
243,W,G	-10.543
244,N,S	-4.515
313,M,T	-4.717
314,F,L	-5.695
>ND3
27,L,P	-4.852
>ND4
>ND4L
>ND5
226,Y,S	-4.280
241,N,K	-4.765
301,K,M	-4.482
>ND6
