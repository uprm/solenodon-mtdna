#!/usr/bin/env python
from sys import argv, exit, stdin, stdout

are_bed_fields = lambda f: (f[0].isdigit() and f[1].isdigit())
are_long_fields = lambda f: (len(f[2] + f[3]) > 2)
is_gap = lambda s: (s == "-" * len(s))

def breakup(fields, i, max_i):
    if is_gap(fields[2]):
        yield "\t".join(fields) + "\n"
    elif is_gap(fields[3]) and ((i == 1) or (i == max_i)):
        return
    else:
        stretch = range(int(fields[0]), int(fields[1]))
        for i, coordinate in enumerate(stretch):
            substitute_fields = [
                str(coordinate),
                str(coordinate + 1),
                fields[2][i],
                fields[3][i]
            ]
            yield "\t".join(substitute_fields) + "\n"

def main(argv=argv):
    lines = stdin.readlines()
    max_i = len(lines) - 1
    for i, line in enumerate(lines):
        fields = line.split()
        if are_bed_fields(fields):
            if are_long_fields(fields):
                for substitute in breakup(fields, i, max_i):
                    stdout.write(substitute)
            else:
                stdout.write(line)
        else:
            stdout.write(line)
    return 0

if __name__ == "__main__":
    exit(main(argv))
