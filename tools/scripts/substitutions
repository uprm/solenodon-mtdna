#!/usr/bin/env python
from sys import argv, exit
from collections import defaultdict

codon_table = {
    "TTT": "Phe", "TTC": "Phe", "TTA": "Leu", "TTG": "Leu", "CTT": "Leu",
    "CTC": "Leu", "CTA": "Leu", "CTG": "Leu", "ATT": "Ile", "ATC": "Ile",
    "ATA": "Met", "ATG": "Met", "GTT": "Val", "GTC": "Val", "GTA": "Val",
    "GTG": "Val", "TCT": "Ser", "TCC": "Ser", "TCA": "Ser", "TCG": "Ser",
    "CCT": "Pro", "CCC": "Pro", "CCA": "Pro", "CCG": "Pro", "ACT": "Thr",
    "ACC": "Thr", "ACA": "Thr", "ACG": "Thr", "GCT": "Ala", "GCC": "Ala",
    "GCA": "Ala", "GCG": "Ala", "TAT": "Tyr", "TAC": "Tyr", "TAA": "***",
    "TAG": "***", "CAT": "His", "CAC": "His", "CAA": "Gln", "CAG": "Gln",
    "AAT": "Asn", "AAC": "Asn", "AAA": "Lys", "AAG": "Lys", "GAT": "Asp",
    "GAC": "Asp", "GAA": "Glu", "GAG": "Glu", "TGT": "Cys", "TGC": "Cys",
    "TGA": "Trp", "TGG": "Trp", "CGT": "Arg", "CGC": "Arg", "CGA": "Arg",
    "CGG": "Arg", "AGT": "Ser", "AGC": "Ser", "AGA": "***", "AGG": "***",
    "GGT": "Gly", "GGC": "Gly", "GGA": "Gly", "GGG": "Gly", "<->": "<->"
}

short = {
    "Ala": "A", "Asx": "B", "Cys": "C", "Asp": "D", "Glu": "E",
    "Phe": "F", "Gly": "G", "His": "H", "Ile": "I", "Lys": "K",
    "Leu": "L", "Met": "M", "Asn": "N", "Pro": "P", "Gln": "Q",
    "Arg": "R", "Ser": "S", "Thr": "T", "Val": "V", "Trp": "W",
    "Xaa": "X", "Tyr": "Y", "Glx": "Z", "***": "*", "<->": "."
}

def read_from(filename):
    with open(filename) as data:
        return [s.strip() for s in data.readlines()]

def flanking_nucleoseq(line):
    fields = line.split(" : ")
    if (len(fields) == 3) and fields[0].strip().isdigit() and fields[2].strip().isdigit():
        return fields[1].upper()
    return None

def chew(seq):
    for i in range(0, len(seq), 3):
        yield seq[i:i+3]

def translate(seq):
    for codon in chew(seq):
        yield codon_table.get(codon, "???")

def parse(contents):
    query, target = [], []
    for i in range(len(contents) - 5):
        A = flanking_nucleoseq(contents[i]) 
        B = flanking_nucleoseq(contents[i+4])
        if A and B:
            query.append(A)
            target.append(B)
    return (
        "".join(translate("".join(query))),
        "".join(translate("".join(target)))
    )

def count_substitutions(query, target):
    substitutions = defaultdict(dict)
    for Q, T in zip(chew(query), chew(target)):
        if Q != T:
            substitutions[Q][T] = 1 + substitutions[Q].get(T, 0)
    return substitutions

def flatten(substitutions):
    flattened = {}
    for Q in substitutions.keys():
        for T in substitutions[Q].keys():
            flattened[Q + " -> " + T] = substitutions[Q][T]
    return flattened

def show_each(contents):
    query_name, buf = "", []
    for line in contents:
        if line[:7] in ("Query: ", "-- comp"):
            if query_name and buf:
                query, target = parse(buf)
                yield query_name, query, target
                query_name, buf = "", []
            query_name = line[7:]
        else:
            buf.append(line.strip())

def shortened(seq):
    for aa in chew(seq):
        yield short.get(aa, "?")

def call(query, target):
    for p, (Q, T) in enumerate(zip(query, target)):
        if Q != T:
            yield p, Q, T

def main(argv=argv):
    contents = read_from(argv[2])
    if argv[1] == "count":
        query, target = parse(contents)
        substitutions = count_substitutions(query, target)
        for k, v in flatten(substitutions).items():
            print(k, v)
    elif argv[1] == "show":
        for query_name, query, target in show_each(contents):
            print(">" + query_name)
            print(query)
            print(target)
    elif argv[1] == "store":
        directory = argv[3]
        for query_name, query, target in show_each(contents):
            filename = query_name.split()[0]
            query = "".join(shortened(query))
            target = "".join(shortened(target))
            if (len(argv) > 4) and (argv[4] == "flip"):
                query, target = target, query
            with open("{}/{}.fa".format(directory, filename), "w") as fasta:
                fasta.write(">{}\n{}\n".format(query_name, query))
            with open("{}/{}.vr".format(directory, filename), "w") as variants:
                for position, ref, alt in call(query, target):
                    if (ref != "?") and (alt != "?"):
                        variants.write("{},{},{}\n".format(position + 1, ref, alt))

if __name__ == "__main__":
    exit(main(argv))
