export
proot = $(shell pwd)
include config/variables.mk
include config/housekeeping.mk

target: ; $(MAKE) circos.png

%.png: %.xml
	tools/scripts/specific-fixes
	$(circos) -conf $< -outputfile $(shell echo $@ | cut -d. -f1)
	rm -f "$(shell echo $@ | cut -d. -f1).svg" || :

circos.xml: $(track_xmls) $(sanger_xmls)
	cat config/circos-head.xml > $@
	echo $^ | tr ' ' '\n' | sed "s/^/\t<<include /g" | sed "s/$$/>>/g" >> $@
	cat config/circos-tail.xml >> $@

tracks/%.xml: data/mtdna/%.fa
	$(eval base_radius := $(shell $(make_radius) $@))
	tools/scripts/wrap-mafft \
		$(reference) $< \
		| tools/scripts/flatten-fasta \
		> $<.tmp
	tools/scripts/mafft2bed \
		< $<.tmp \
		| tools/scripts/breakbed \
		| tools/scripts/bed2pretrack \
		> $<.tmp.pt
	tools/scripts/pt2tracks \
		$<.tmp.pt \
		"tracks" \
		$(base_radius)
	tools/scripts/get-span \
		$<.tmp \
		"tracks"

tracks/sanger/%.xml: data/reads/%_trimmed.fq
	$(eval base_radius := $(shell $(make_radius) $@))
	mkdir "tracks/sanger" || :
	$(bwa) mem $(reference) $< \
		| samtools view -b - \
		> $<.tmp.ubam
	samtools sort \
		$<.tmp.ubam $<.tmp
	samtools mpileup \
		-uf $(reference) $<.tmp.bam \
		| bcftools call -mvOv - \
		> $<.tmp.vcf
	tools/scripts/vcf2pretrack \
		< $<.tmp.vcf \
		> $<.tmp.pt
	tools/scripts/pt2tracks \
		$<.tmp.pt \
		"tracks/sanger" \
		$(base_radius)
	bedtools bamtobed -i $<.tmp.bam \
		| tools/scripts/bed2span "tracks/sanger" $<

clean:
	rm -rf $(tmp)
	rm -f $(tracks)/*.*
	rm -f $(tracks)/sanger/*.*
	rm -f data/mtdna/*.fa.tmp*
	rm -f data/reads/*.fq.tmp*
	rm -f circos.??g
	rm -f circos.xml
